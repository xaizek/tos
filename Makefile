CXX  := g++
AS   := gcc
LD   := gcc
QEMU := qemu-system-i386


CXXFLAGS  := -m32 -std=c++11 -g -MMD -Isrc/
CXXFLAGS  += -Wall -Wextra -Werror
CXXFLAGS  += -fno-use-cxa-atexit -fno-builtin -fno-rtti -fno-exceptions
CXXFLAGS  += -fno-threadsafe-statics
ASFLAGS   := -m32
LINKFLAGS := -g -m32 -nostartfiles -nostdlib
QEMUFLAGS := -netdev user,id=n0,hostfwd=udp::1234-:1234,hostfwd=tcp::1234-:1234
QEMUFLAGS += -device rtl8139,netdev=n0,mac=12:23:34:45:56:77
QEMUFLAGS += -drive file=./hda,index=0,media=disk,format=raw


# traverse directories ($1) recursively looking for patterns ($2) to make list
# of matching files
rwildcard = $(foreach d,$(wildcard $1*), \
              $(foreach p,$2, $(call rwildcard,$d/,$p) \
                              $(filter $(subst *,%,$p),$d)))


src     := $(call rwildcard, src/, *.cpp *.s *.S)
objects := $(patsubst src/%,obj/%.o,$(basename $(src)))
depends := $(objects:.o=.d)
dirs    := $(sort $(dir $(objects)))


obj/%.o: src/%.cpp | $(dirs)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

obj/%.o: src/%.s | $(dirs)
	$(AS) $(ASFLAGS) -c -o $@ $<

obj/%.o: src/%.S | $(dirs)
	$(AS) $(ASFLAGS) -c -o $@ $<

kernel: linker.ld $(objects)
	$(CXX) $(LINKFLAGS) -Wl,-T $< -o $@ $(objects)

$(dirs):
	mkdir -p $@

run: kernel
	$(QEMU) -kernel $< $(QEMUFLAGS) &

udp-client:
	@echo '[[[ Connecting to UDP server inside virtual machine... ]]]'
	nc -u 127.0.0.1 1234

udp-server:
	@echo '[[[ Opening UDP socket for kernel to connect to... ]]]'
	nc -u 127.0.0.1 -lp 1235

tcp-client:
	@echo '[[[ Connecting to TCP server inside virtual machine... ]]]'
	nc 127.0.0.1 1234

tcp-server:
	@echo '[[[ Opening TCP socket for kernel to connect to... ]]]'
	nc 127.0.0.1 -lp 1235

gdb: kernel
	@gdb -quiet -se=$< \
	     -ex 'target remote | exec $(QEMU) -gdb stdio -kernel $< $(QEMUFLAGS)'

clean:
	$(RM) -r kernel obj/

.PHONY: run udp-client udp-server tcp-client tcp-server gdb clean

-include $(depends)
