#ifndef TOS__PRINT_HPP__
#define TOS__PRINT_HPP__

#include <cstdint>

namespace hwcomm {
    class Screen;
}

namespace detail {

void kprint(const char str[]);
void kprint(std::uint8_t byte);
void kprint(std::uint16_t word);
void kprint(std::uint32_t dword);
void kprint(std::uint64_t dword);
void kprint(void *ptr);

}

hwcomm::Screen & getScreen();

template <typename ...Args>
void
kprint(Args ...args)
{
    const bool ignored[] = { (detail::kprint({ args }), false)... };
    static_cast<void>(ignored);
}

#endif // TOS__PRINT_HPP__
