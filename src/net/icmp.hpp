#ifndef TOS__NET__ICMP_HPP__
#define TOS__NET__ICMP_HPP__

#include <cstdint>

#include "net/ipv4.hpp"
#include "utils/struct.hpp"

namespace net {

struct ICMPMsg
{
    NetField<+0, 1, std::uint8_t> type;
    NetField<+1, 1, std::uint8_t> code;

    NetField<+2, 2, std::uint16_t> checksum;
    NetField<+4, 4, std::uint32_t> data;

    explicit ICMPMsg(std::uint8_t data[])
        : type(data), code(data), checksum(data), data(data)
    {
    }

    static constexpr std::ptrdiff_t size()
    {
        using lastField = decltype(data);
        return lastField::offset + lastField::size;
    }
};

class ICMP : private IPHandler
{
public:
    ICMP(IPProvider &backend);

public:
    void requestEchoReply(NetOrder<std::uint32_t> ip);

private:
    virtual bool onIPReceived(NetOrder<std::uint32_t> srcIP,
                              NetOrder<std::uint32_t> dstIP,
                              span<std::uint8_t> msg) override;
};

}

#endif // TOS__NET__ICMP_HPP__
