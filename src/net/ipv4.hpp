#ifndef TOS__NET__IPV4_HPP__
#define TOS__NET__IPV4_HPP__

#include "net/arp.hpp"
#include "net/etherframe.hpp"
#include "utils/span.hpp"
#include "utils/struct.hpp"

namespace net {

struct IPv4Msg
{
    NetField<+0, 1, std::uint8_t> verAndHdrLen;
    NetField<+1, 1, std::uint8_t> tos;
    NetField<+2, 2, std::uint16_t> totalLength;

    NetField<+4, 2, std::uint16_t> ident;
    NetField<+6, 2, std::uint16_t> flagsAndOffset;

    NetField<+8, 1, std::uint8_t> timeToLive;
    NetField<+9, 1, std::uint8_t> protocol;
    NetField<+10, 2, std::uint16_t> checksum;

    NetField<+12, 4, std::uint32_t> srcIP;
    NetField<+16, 4, std::uint32_t> dstIP;

    explicit IPv4Msg(std::uint8_t data[])
        : verAndHdrLen(data), tos(data), totalLength(data), ident(data),
          flagsAndOffset(data), timeToLive(data), protocol(data),
          checksum(data), srcIP(data), dstIP(data)
    {
    }

    static constexpr std::ptrdiff_t size()
    {
        using lastField = decltype(dstIP);
        return lastField::offset + lastField::size;
    }
};

class IPProvider;

class IPHandler
{
protected:
    IPProvider &backend;
    std::uint8_t protocol;

public:
    IPHandler(IPProvider &backend, std::uint8_t protocol);
    ~IPHandler();

    virtual bool onIPReceived(NetOrder<std::uint32_t> srcIP,
                              NetOrder<std::uint32_t> dstIP,
                              span<std::uint8_t> msg) = 0;
    void send(NetOrder<std::uint32_t> dstIP, span<const std::uint8_t> msg);
};

class IPProvider : public EtherFrameHandler
{
    friend class IPHandler;

public:
    IPProvider(EtherFrameProvider &backend, ARP &arp,
               NetOrder<std::uint32_t> gatewayIP,
               NetOrder<std::uint32_t> subnetMask);

public:
    void send(NetOrder<std::uint32_t> dstIP, std::uint8_t protocol,
              span<const std::uint8_t> msg);

public:
    static NetOrder<std::uint16_t> checksum(span<const std::uint8_t> data);

private:
    virtual bool onEtherFrameReceived(span<std::uint8_t> msg) override;

private:
    IPHandler *handlers[255];
    ARP &arp;
    NetOrder<std::uint32_t> gatewayIP;
    NetOrder<std::uint32_t> subnetMask;
    std::uint16_t nextIdent;
};

}

#endif // TOS__NET__IPV4_HPP__
