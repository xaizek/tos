#include "net/icmp.hpp"

#include "print.hpp"

using namespace net;

ICMP::ICMP(IPProvider &backend) : IPHandler(backend, 0x01)
{
}

bool
ICMP::onIPReceived(NetOrder<std::uint32_t> srcIP,
                   NetOrder<std::uint32_t> /*dstIP*/,
                   span<std::uint8_t> msg)
{
    if (msg.size() < ICMPMsg::size()) {
        return false;
    }

    ICMPMsg icmpMsg(msg.data());

    switch (icmpMsg.type.BE) {
        case 0:
            kprint("ping response from ", srcIP.BE, "\n");
            break;

        case 8:
            icmpMsg.type = netOrder<std::uint8_t>(0);
            icmpMsg.checksum = netOrder<std::uint16_t>(0);
            icmpMsg.checksum = IPProvider::checksum(msg.first(ICMPMsg::size()));
            return true;
    }

    return false;
}

void
ICMP::requestEchoReply(NetOrder<std::uint32_t> ip)
{
    std::uint8_t data[ICMPMsg::size()];
    ICMPMsg icmp(data);
    icmp.type = netOrder<std::uint8_t>(8); // ping
    icmp.code = netOrder<std::uint8_t>(0);
    icmp.data = toNetOrder<std::uint32_t>(0x13370000);
    icmp.checksum = netOrder<std::uint16_t>(0);
    icmp.checksum = IPProvider::checksum(data);

    send(ip, data);
}
