#ifndef TOS__NET__ETHERFRAME_HPP__
#define TOS__NET__ETHERFRAME_HPP__

#include "drv/drivers.hpp"
#include "utils/span.hpp"
#include "utils/struct.hpp"

namespace net {

static constexpr NetOrder<std::uint64_t> broadcastMAC(0xffffffffffff);

struct EtherFrameHeader
{
    NetField<+0,  6, std::uint64_t> dstMAC;
    NetField<+6,  6, std::uint64_t> srcMAC;
    NetField<+12, 2, std::uint16_t> etherType;

    explicit EtherFrameHeader(std::uint8_t data[])
        : dstMAC(data), srcMAC(data), etherType(data)
    {
    }

    static constexpr std::ptrdiff_t size()
    {
        using lastField = decltype(etherType);
        return lastField::offset + lastField::size;
    }
};

using EtherFrameFooter = std::uint32_t;

class EtherFrameProvider;

class EtherFrameHandler
{
public:
    EtherFrameHandler(EtherFrameProvider &backend, std::uint16_t etherType);
    ~EtherFrameHandler();

public:
    void send(NetOrder<std::uint64_t> dstMAC, span<const std::uint8_t> msg);

    NetOrder<std::uint32_t> getIPAddress() const;

public:
    virtual bool onEtherFrameReceived(span<std::uint8_t> msg) = 0;

protected:
    EtherFrameProvider &backend;
    NetOrder<std::uint16_t> etherType;
};

class EtherFrameProvider : public drv::RawDataHandler
{
    friend class EtherFrameHandler;

public:
    EtherFrameProvider(drv::EthernetDriver &backend);

public:
    void send(NetOrder<std::uint64_t> dstMAC, NetOrder<std::uint16_t> etherType,
              span<const std::uint8_t> msg);

    NetOrder<std::uint64_t> getMACAddress() const;
    NetOrder<std::uint32_t> getIPAddress() const;

private:
    virtual bool onRawDataReceived(span<std::uint8_t> msg) override;

private:
    EtherFrameHandler *handlers[65535];
};

}

#endif // TOS__NET__ETHERFRAME_HPP__
