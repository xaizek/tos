#ifndef TOS__NET__ARP_HPP__
#define TOS__NET__ARP_HPP__

#include "net/etherframe.hpp"
#include "utils/struct.hpp"

namespace net {

struct ARPMsg
{
    NetField<+0, 2, std::uint16_t> hardwareType;
    NetField<+2, 2, std::uint16_t> protocol;
    NetField<+4, 1, std::uint8_t> hardwareAddressSize; // 6
    NetField<+5, 1, std::uint8_t> protocolAddressSize; // 4
    NetField<+6, 2, std::uint16_t> command;

    NetField<+8,  6, std::uint64_t> srcMAC;
    NetField<+14, 4, std::uint32_t> srcIP;
    NetField<+18, 6, std::uint64_t> dstMAC;
    NetField<+24, 4, std::uint32_t> dstIP;

    explicit ARPMsg(std::uint8_t data[])
        : hardwareType(data), protocol(data), hardwareAddressSize(data),
          protocolAddressSize(data), command(data), srcMAC(data), srcIP(data),
          dstMAC(data), dstIP(data)
    {
    }

    static constexpr std::ptrdiff_t size()
    {
        using lastField = decltype(dstIP);
        return lastField::offset + lastField::size;
    }
};

class ARP : public EtherFrameHandler
{
public:
    ARP(EtherFrameProvider &backend);

public:
    void requestMACAddress(NetOrder<std::uint32_t> ip);
    void broadcastMACAddress(NetOrder<std::uint32_t> ip);
    NetOrder<std::uint64_t> getMACFromCache(NetOrder<std::uint32_t> ip);
    NetOrder<std::uint64_t> resolve(NetOrder<std::uint32_t> ip);

private:
    virtual bool onEtherFrameReceived(span<std::uint8_t> msg) override;

private:
    static constexpr int maxCacheEntries = 128;
    NetOrder<std::uint32_t> IPcache[maxCacheEntries];
    NetOrder<std::uint64_t> MACcache[maxCacheEntries];
    int numCacheEntries;
};

}

#endif // TOS__NET__ARP_HPP__
