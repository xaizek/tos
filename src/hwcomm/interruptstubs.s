.set IrqBase, 0x20

.section .text

.extern handleInterrupt

.macro HandleException num
.global HandleException\num\()
HandleException\num\():
    movb $\num, (interruptnumber)
    jmp int_bottom
.endm

.macro HandleIRQ num
.global HandleIRQ\num\()
HandleIRQ\num\():
    movb $\num + IrqBase, (interruptnumber)
    jmp int_bottom
.endm

HandleException 0x00
HandleException 0x01
HandleException 0x02
HandleException 0x03
HandleException 0x04
HandleException 0x05
HandleException 0x06
HandleException 0x07
HandleException 0x08
HandleException 0x09
HandleException 0x0A
HandleException 0x0B
HandleException 0x0C
HandleException 0x0D
HandleException 0x0E
HandleException 0x0F
HandleException 0x10
HandleException 0x11
HandleException 0x12
HandleException 0x13

HandleException 0x80

HandleIRQ 0x00
HandleIRQ 0x01
HandleIRQ 0x02
HandleIRQ 0x03
HandleIRQ 0x04
HandleIRQ 0x05
HandleIRQ 0x06
HandleIRQ 0x07
HandleIRQ 0x08
HandleIRQ 0x09
HandleIRQ 0x0A
HandleIRQ 0x0B
HandleIRQ 0x0C
HandleIRQ 0x0D
HandleIRQ 0x0E
HandleIRQ 0x0F
HandleIRQ 0x31

int_bottom:

    # save registers
    pushl %ds
    pushl %es
    pushl %fs
    pushl %gs

    pushl %ebp
    pushl %edi
    pushl %esi

    pushl %edx
    pushl %ecx
    pushl %ebx
    pushl %eax

    # call C++ handler
    pushl %esp
    push (interruptnumber)
    call handleInterrupt
    add %esp, 6
    # switch the stack
    mov %eax, %esp

    # restore registers
    popl %eax
    popl %ebx
    popl %ecx
    popl %edx

    popl %esi
    popl %edi
    popl %ebp
    pop %gs
    pop %fs
    pop %es
    pop %ds

.global InterruptIgnore
InterruptIgnore:
    iret

.global irqBase
.data
    interruptnumber: .byte 0
    irqBase: .int IrqBase
