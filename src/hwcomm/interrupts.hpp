#ifndef TOS__HWCOMM__INTERRUPTS_HPP__
#define TOS__HWCOMM__INTERRUPTS_HPP__

#include <cstdint>

class GlobalDescriptorTable;

namespace hwcomm {

class InterruptHandler
{
protected:
    InterruptHandler(std::uint8_t interruptNumber);
    ~InterruptHandler();

public:
    virtual std::uint32_t handleInterrupt(std::uint32_t esp) = 0;

private:
    std::uint8_t interruptNumber;
};

class InterruptManager
{
public:
    explicit InterruptManager(GlobalDescriptorTable &gdt);
    ~InterruptManager();

public:
    std::uint16_t getIRQBase() const;
    void activate();
    void deactivate();
};

}

#endif // TOS__HWCOMM__INTERRUPTS_HPP__
