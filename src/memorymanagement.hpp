#ifndef TOS__MEMORYMANAGEMENT_HPP__
#define TOS__MEMORYMANAGEMENT_HPP__

#include <cstddef>

class MemoryManager
{
    struct MemoryChunk;

public:
    MemoryManager(void *area, std::size_t size);
    MemoryManager(const MemoryManager &lhs) = delete;
    MemoryManager & operator=(const MemoryManager &lhs) = delete;
    ~MemoryManager();

public:
    void * malloc(std::size_t size);
    void free(void *ptr);

private:
    MemoryChunk *head;
};

#endif // TOS__MEMORYMANAGEMENT_HPP__
