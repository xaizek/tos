#ifndef TOS__GUI__WINDOW_HPP__
#define TOS__GUI__WINDOW_HPP__

#include <cstdint>

#include "drv/mouse.hpp"
#include "gui/Widget.hpp"

namespace gui {

class Window : public CompositeWidget
{
public:
    Window(Widget *parent, Position pos, Dimentions size, Color color);

public:
    virtual void onMouseDown(Position p, std::uint8_t button) override;
    virtual void onMouseUp(Position p, std::uint8_t button) override;
    virtual void onMouseMove(Position oldPos, Position newPos) override;

private:
    bool dragging;
};

}

#endif // TOS__GUI__WINDOW_HPP__
