#ifndef TOS__GUI__WIDGET_HPP__
#define TOS__GUI__WIDGET_HPP__

#include <cstdint>

#include "drv/keyboard.hpp"
#include "drv/VGA.hpp"

namespace gui {

using GraphicsContext = drv::VGA;

struct Position
{
    int x;
    int y;
};

Position operator-(const Position &lhs, const Position &rhs);
Position & operator+=(Position &lhs, const Position &rhs);

struct Dimentions
{
    int w;
    int h;
};

struct Color
{
    std::uint8_t r;
    std::uint8_t g;
    std::uint8_t b;
};

class Widget : public drv::KeyboardEventHandler
{
protected:
    Widget(Widget *parent, Position pos, Dimentions size, Color color);

public:
    virtual void focus(Widget &widget);
    virtual Position modelToScreen(Position p);
    virtual bool containsCoordinate(Position p);

    virtual void draw(GraphicsContext &gc);
    virtual void onMouseDown(Position p, std::uint8_t button);
    virtual void onMouseUp(Position p, std::uint8_t button);
    virtual void onMouseMove(Position oldPos, Position newPos);

protected:
    Widget *parent;
    Position pos;
    Dimentions size;
    Color color;
    bool focusable;
};

class CompositeWidget : public Widget
{
public:
    CompositeWidget(Widget *parent, Position pos, Dimentions size, Color color);

public:
    virtual bool addChild(Widget &child);

    virtual void focus(Widget &widget) override;

    virtual void draw(GraphicsContext &gc) override;
    virtual void onMouseDown(Position p, std::uint8_t button) override;
    virtual void onMouseUp(Position p, std::uint8_t button) override;
    virtual void onMouseMove(Position oldPos, Position newPos) override;

    virtual void onKeyDown(std::uint8_t scanCode) override;
    virtual void onKeyUp(std::uint8_t scanCode) override;

private:
    Widget *children[100];
    int numChildren;
    Widget *focusedChild;
};

}

#endif // TOS__GUI__WIDGET_HPP__
