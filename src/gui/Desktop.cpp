#include "gui/Desktop.hpp"

#include <cstdint>

using namespace gui;

Desktop::Desktop(Dimentions size, Color color)
    : CompositeWidget(nullptr, Position { 0, 0 }, size, color)
{
    mouseX = size.w/2;
    mouseY = size.h/2;
}

void
Desktop::draw(GraphicsContext &gc)
{
    CompositeWidget::draw(gc);

    drv::Color white = { 0xff, 0xff, 0xff };
    for (int i = 0; i < 4; ++i) {
        gc.putPixel({ mouseX - i, mouseY }, white);
        gc.putPixel({ mouseX + i, mouseY }, white);
        gc.putPixel({ mouseX, mouseY - i }, white);
        gc.putPixel({ mouseX, mouseY + i }, white);
    }
}

void
Desktop::onMouseDown(std::uint8_t button)
{
    CompositeWidget::onMouseDown({ mouseX, mouseY }, button);
}

void
Desktop::onMouseUp(std::uint8_t button)
{
    CompositeWidget::onMouseUp({ mouseX, mouseY }, button);
}

void Desktop::onMouseMove(int dx, int dy)
{
    dx /= 2;
    dy /= 2;

    int newMouseX = mouseX + dx;
    if (newMouseX < 0) {
        newMouseX = 0;
    }
    if (newMouseX >= size.w) {
        newMouseX = size.w - 1;
    }

    int newMouseY = mouseY + dy;
    if (newMouseY < 0) {
        newMouseY = 0;
    }
    if (newMouseY >= size.h) {
        newMouseY = size.h - 1;
    }

    Position oldPos = { mouseX, mouseY };
    Position newPos = { newMouseX, newMouseY };
    CompositeWidget::onMouseMove(oldPos, newPos);

    mouseX = newMouseX;
    mouseY = newMouseY;
}
