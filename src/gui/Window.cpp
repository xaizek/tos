#include "gui/Window.hpp"

#include <cstdint>

using namespace gui;

Window::Window(Widget *parent, Position pos, Dimentions size, Color color)
    : CompositeWidget(parent, pos, size, color), dragging(false)
{
}

void
Window::onMouseDown(Position p, std::uint8_t button)
{
    dragging = (button == 1);
    CompositeWidget::onMouseDown(p, button);
}

void
Window::onMouseUp(Position p, std::uint8_t button)
{
    dragging = false;
    CompositeWidget::onMouseUp(p, button);
}

void
Window::onMouseMove(Position oldPos, Position newPos)
{
    if (dragging) {
        pos += newPos - oldPos;
    } else {
        CompositeWidget::onMouseMove(oldPos, newPos);
    }
}
