#ifndef TOS__GUI__DESKTOP_HPP__
#define TOS__GUI__DESKTOP_HPP__

#include <cstdint>

#include "drv/mouse.hpp"
#include "gui/Widget.hpp"

namespace gui {

class Desktop : public CompositeWidget, public drv::MouseEventHandler
{
public:
    Desktop(Dimentions size, Color color);

public:
    virtual void draw(GraphicsContext &gc) override;

    virtual void onMouseDown(std::uint8_t button) override;
    virtual void onMouseUp(std::uint8_t button) override;
    virtual void onMouseMove(int dx, int dy) override;

private:
    int mouseX;
    int mouseY;
};

}

#endif // TOS__GUI__DESKTOP_HPP__
