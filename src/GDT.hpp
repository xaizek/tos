#ifndef TOS__GDT_HPP__
#define TOS__GDT_HPP__

#include <cstdint>

class GlobalDescriptorTable
{
public:
    GlobalDescriptorTable();

public:
    std::uint16_t getCS() const;
    std::uint16_t getDS() const;
};

#endif // TOS__GDT_HPP__
