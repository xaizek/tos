#ifndef TOS__UTILS__CONV_HPP__
#define TOS__UTILS__CONV_HPP__

#include <cstdint>

constexpr std::uint16_t
ntoh(std::uint16_t word)
{
    return ((word & 0x00ff) << 8)
         | ((word & 0xff00) >> 8);
}

constexpr std::uint32_t
ntoh(std::uint32_t dword)
{
    return ((dword & 0x000000ff) << 24)
         | ((dword & 0x0000ff00) << 8)
         | ((dword & 0x00ff0000) >> 8)
         | ((dword & 0xff000000) >> 24);
}

constexpr std::uint16_t
hton(std::uint16_t word)
{
    return ntoh(word);
}

constexpr std::uint32_t
hton(std::uint32_t dword)
{
    return ntoh(dword);
}

#endif // TOS__UTILS__CONV_HPP__
