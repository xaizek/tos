#include "syscalls.hpp"

#include <new>

#include "hwcomm/interrupts.hpp"
#include "print.hpp"

SyscallHandler::SyscallHandler(std::uint8_t interruptNumber)
    : InterruptHandler(interruptNumber)
{
}

std::uint32_t
SyscallHandler::handleInterrupt(std::uint32_t esp)
{
    auto cpuState = new(reinterpret_cast<void *>(esp)) CPUState;

    switch (cpuState->eax()) {
        case 4:
            kprint(reinterpret_cast<const char *>(cpuState->ebx()));
            break;
    }

    return esp;
}
