#ifndef TOS__ASSERT_HPP__
#define TOS__ASSERT_HPP__

#include <utility>

#include "print.hpp"

extern "C" void hang();

template <typename ...Args>
void
kassert(bool check, Args &&...args)
{
    if (!check) {
        kprint(std::forward<Args>(args)..., "\n");
        hang();
    }
}

#endif // TOS__ASSERT_HPP__
