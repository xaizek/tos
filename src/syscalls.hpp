#ifndef TOS__SYSCALLS_HPP__
#define TOS__SYSCALLS_HPP__

#include <cstdint>

#include "hwcomm/interrupts.hpp"
#include "multitasking.hpp"

class SyscallHandler : public hwcomm::InterruptHandler
{
public:
    SyscallHandler(std::uint8_t interruptNumber);

private:
    virtual std::uint32_t handleInterrupt(std::uint32_t esp) override;
};

#endif // TOS__SYSCALLS_HPP__
