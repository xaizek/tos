#ifndef TOS__DRIVERS__ATA_HPP__
#define TOS__DRIVERS__ATA_HPP__

#include <cstdint>

#include "hwcomm/Port.hpp"
#include "hwcomm/interrupts.hpp"
#include "utils/span.hpp"

namespace drv {

class ATA
{
public:
    ATA(bool master, std::uint16_t portBase);

public:
    void identify();
    void read28(std::uint32_t sectorNum, span<std::uint8_t> buf);
    void write28(std::uint32_t sectorNum, span<const std::uint8_t> buf);
    void flush();

private:
    void waitWhileBusy();
    // Returns `false` on error.
    bool waitUntilReady();

private:
    hwcomm::Port16 dataPort;
    hwcomm::Port8 errorPort;
    hwcomm::Port8 sectorCountPort;
    hwcomm::Port8 lbaLowPort;
    hwcomm::Port8 lbaMidPort;
    hwcomm::Port8 lbaHiPort;
    hwcomm::Port8 devicePort;
    hwcomm::Port8 commandPort;
    hwcomm::Port8 statusPort;
    hwcomm::Port8 controlPort;

    std::uint8_t devBit;
};

}

#endif // TOS__DRIVERS__ATA_HPP__
