#ifndef TOS__DRV__RTL8139_HPP__
#define TOS__DRV__RTL8139_HPP__

#include <cstdint>

#include "drv/drivers.hpp"
#include "hwcomm/Port.hpp"
#include "hwcomm/interrupts.hpp"

namespace hwcomm {
    class PCIDeviceDescr;
}

namespace drv {

class rtl8139 : public EthernetDriver, public hwcomm::InterruptHandler
{
public:
    rtl8139(hwcomm::PCIDeviceDescr &dev,
            hwcomm::InterruptManager &interrupts);

public:
    virtual void send(span<const std::uint8_t> msg) override;
    virtual void receive() override;
    virtual NetOrder<std::uint64_t> getMAC() const override;

private:
    virtual void activate() override;
    virtual int reset() override;
    virtual const char * getName() const override;

private:
    virtual std::uint32_t handleInterrupt(std::uint32_t esp) override;

private:
    void received();

private:
    hwcomm::Port32 macLowPort;
    hwcomm::Port32 macHighPort;
    hwcomm::Port32 marLowPort;
    hwcomm::Port32 marHighPort;
    hwcomm::Port32 rbstartPort;
    hwcomm::Port8 cmdPort;
    hwcomm::Port16 caprPort;
    hwcomm::Port16 imrPort;
    hwcomm::Port16 isrPort;
    hwcomm::Port32 tcrPort;
    hwcomm::Port32 rcrPort;
    hwcomm::Port8 cfgPort;
    hwcomm::Port32 tsdPort[4];
    hwcomm::Port32 tsadPort[4];

    int currentSendDescr;
    std::uint64_t mac;

    std::uint8_t *recvBuffer;
    std::uint8_t recvBufferStorage[8192 + 16 + 1500];

    std::uint8_t *sendBuffer[4];
    std::uint8_t sendBufferStorage[4][2000];
};

}

#endif // TOS__DRV__RTL8139_HPP__
