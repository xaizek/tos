#ifndef TOS__DRV__DRIVERS_HPP__
#define TOS__DRV__DRIVERS_HPP__

#include <cstdint>

#include "utils/span.hpp"
#include "utils/struct.hpp"

namespace drv {

class Driver
{
public:
    virtual ~Driver() = default;

public:
    virtual void activate();
    virtual int reset();
    virtual void deactivate();
    virtual const char * getName() const = 0;
};

class RawDataHandler;

class EthernetDriver : public Driver
{
public:
    // `handler` can be `nullptr`.
    void setHandler(RawDataHandler *handler);

    void setIPAddress(NetOrder<std::uint32_t> ip);
    NetOrder<std::uint32_t> getIPAddress() const;

public:
    virtual void send(span<const std::uint8_t> msg) = 0;
    virtual void receive() = 0;
    virtual NetOrder<std::uint64_t> getMAC() const = 0;

protected:
    RawDataHandler *const &handler = handlerValue;

private:
    RawDataHandler *handlerValue = nullptr;
    NetOrder<std::uint32_t> ipAddress { 0U };
};

class RawDataHandler
{
public:
    RawDataHandler(EthernetDriver &backend);
    ~RawDataHandler();

public:
    void send(span<const std::uint8_t> msg);

public:
    virtual bool onRawDataReceived(span<std::uint8_t> msg) = 0;

protected:
    EthernetDriver &backend;
};

class DriverManager
{
public:
    explicit DriverManager();

public:
    void addDriver(Driver &drv);
    void activateAll();

    Driver ** begin() { return drivers; }
    Driver ** end() { return drivers + numDrivers; }

private:
    Driver *drivers[256];
    int numDrivers;
};

}

#endif // TOS__DRV__DRIVERS_HPP__
