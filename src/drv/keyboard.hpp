#ifndef TOS__DRV__KEYBOARD_HPP__
#define TOS__DRV__KEYBOARD_HPP__

#include <cstdint>

#include "drv/drivers.hpp"
#include "hwcomm/Port.hpp"
#include "hwcomm/interrupts.hpp"

namespace drv {

class KeyboardEventHandler
{
protected:
    KeyboardEventHandler() = default;
    ~KeyboardEventHandler() = default;

public:
    virtual void onKeyDown(std::uint8_t scanCode);
    virtual void onKeyUp(std::uint8_t scanCode);
};

class KeyboardDriver : public Driver, private hwcomm::InterruptHandler
{
public:
    explicit KeyboardDriver(KeyboardEventHandler &handler);

private:
    virtual void activate() override;
    virtual const char * getName() const override;
    virtual std::uint32_t handleInterrupt(std::uint32_t esp) override;

private:
    hwcomm::Port8 dataPort;
    hwcomm::Port8 cmdPort;
    KeyboardEventHandler &handler;
};

}

#endif // TOS__DRV__KEYBOARD_HPP__
