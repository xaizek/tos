#include "drv/drivers.hpp"

using namespace drv;

void
Driver::activate()
{
}

int
Driver::reset()
{
    return 0;
}

void
Driver::deactivate()
{
}

void
EthernetDriver::setHandler(RawDataHandler *handler)
{
    handlerValue = handler;
}

void
EthernetDriver::setIPAddress(NetOrder<std::uint32_t> ip)
{
    ipAddress = ip;
}

NetOrder<std::uint32_t>
EthernetDriver::getIPAddress() const
{
    return ipAddress;
}

RawDataHandler::RawDataHandler(EthernetDriver &backend) : backend(backend)
{
    backend.setHandler(this);
}

RawDataHandler::~RawDataHandler()
{
    backend.setHandler(nullptr);
}

void
RawDataHandler::send(span<const std::uint8_t> msg)
{
    return backend.send(msg);
}

DriverManager::DriverManager() : numDrivers(0)
{
}

void DriverManager::addDriver(Driver &drv)
{
    drivers[numDrivers++] = &drv;
}

void DriverManager::activateAll()
{
    for (int i = 0; i < numDrivers; ++i) {
        drivers[i]->activate();
    }
}
