#include "drv/keyboard.hpp"

#include "print.hpp"

using namespace drv;

void
KeyboardEventHandler::onKeyDown(std::uint8_t /*scanCode*/)
{
}

void
KeyboardEventHandler::onKeyUp(std::uint8_t /*scanCode*/)
{
}

KeyboardDriver::KeyboardDriver(KeyboardEventHandler &handler)
    : InterruptHandler(0x21), dataPort(0x60), cmdPort(0x64), handler(handler)
{
}

void
KeyboardDriver::activate()
{
    // Drain pending data.
    while (cmdPort.read() & 0x1) {
        dataPort.read();
    }

    // Make sure keyboard is enabled.
    cmdPort.write(0xae);

    // Update controller command byte.
    cmdPort.write(0x20);
    std::uint8_t status = (dataPort.read() | 1) & ~0x10;
    cmdPort.write(0x60);
    dataPort.write(status);

    // Clear output buffer.
    dataPort.write(0xf4);
    // Wait and read ACK.
    while (!(cmdPort.read() & 0x1)) {
        // Wait.
    }
    dataPort.read();
}

const char *
KeyboardDriver::getName() const
{
    return "keyboard";
}

std::uint32_t
KeyboardDriver::handleInterrupt(std::uint32_t esp)
{
    const std::uint8_t scanCode = dataPort.read();
    if (scanCode < 0x80) {
        handler.onKeyDown(scanCode);
    }
    return esp;
}
