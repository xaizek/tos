#ifndef TOS__DRV__VGA_HPP__
#define TOS__DRV__VGA_HPP__

#include <cstdint>

#include <drv/drivers.hpp>
#include <hwcomm/Port.hpp>

namespace drv {

struct Mode
{
    int width;
    int height;
    int colorDepth;
};

struct Position
{
    int x;
    int y;
};

struct Dimentions
{
    int w;
    int h;
};

struct Color
{
    std::uint8_t r;
    std::uint8_t g;
    std::uint8_t b;
};

class VGA
{
public:
    VGA();

public:
    bool supportsMode(const Mode &mode);
    bool setMode(const Mode &mode);

    void beginDrawing();
    void endDrawing();

    void putPixel(Position pos, Color color);
    void fillRectangle(Position pos, Dimentions size, Color color);

private:
    void putPixel(Position pos, std::uint8_t colorIndex);
    void writeRegisters(std::uint8_t *registers);
    std::uint8_t * getFrameBufferSegment();

    std::uint8_t getColorIndex(Color color);

private:
    hwcomm::Port8 miscPort;
    hwcomm::Port8 crtcIndexPort;
    hwcomm::Port8 crtcDataPort;
    hwcomm::Port8 sequencerIndexPort;
    hwcomm::Port8 sequencerDataPort;
    hwcomm::Port8 graphicsControllerIndexPort;
    hwcomm::Port8 graphicsControllerDataPort;
    hwcomm::Port8 attrControllerIndexPort;
    hwcomm::Port8 attrControllerReadPort;
    hwcomm::Port8 attrControllerWritePort;
    hwcomm::Port8 attrControllerResetPort;

    Mode currentMode;

    std::uint8_t buffer[320*200];
};

}

#endif // TOS__DRV__VGA_HPP__
