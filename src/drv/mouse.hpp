#ifndef TOS__DRV__MOUSE_HPP__
#define TOS__DRV__MOUSE_HPP__

#include <cstdint>

#include "drv/drivers.hpp"
#include "hwcomm/Port.hpp"
#include "hwcomm/interrupts.hpp"

namespace drv {

class MouseEventHandler
{
protected:
    MouseEventHandler() = default;
    ~MouseEventHandler() = default;

public:
    virtual void onActivate();
    virtual void onMouseDown(std::uint8_t button);
    virtual void onMouseUp(std::uint8_t button);
    virtual void onMouseMove(int dx, int dy);
};

class MouseDriver : public Driver, private hwcomm::InterruptHandler
{
public:
    explicit MouseDriver(MouseEventHandler &handler);

private:
    virtual void activate() override;
    virtual const char * getName() const override;
    virtual std::uint32_t handleInterrupt(std::uint32_t esp) override;

private:
    hwcomm::Port8 dataPort;
    hwcomm::Port8 cmdPort;

    std::uint8_t buffer[3];
    std::uint8_t offset;
    std::uint8_t buttons;

    MouseEventHandler &handler;
};

}

#endif // TOS__DRV__MOUSE_HPP__
