#ifndef TOS__DRIVERS__AMD_AM79C973_HPP__
#define TOS__DRIVERS__AMD_AM79C973_HPP__

#include <cstdint>

#include "drv/drivers.hpp"
#include "hwcomm/Port.hpp"
#include "hwcomm/interrupts.hpp"

namespace hwcomm {
    class PCIDeviceDescr;
}

namespace drv {

class amd_am79c973 : public EthernetDriver, public hwcomm::InterruptHandler
{
    struct InitializationBlock
    {
        std::uint16_t mode;
        unsigned reserved1 : 4;
        unsigned numSendBuffers : 4;
        unsigned reserved2 : 4;
        unsigned numRecvBuffers : 4;
        std::uint64_t physicalAddress : 48;
        std::uint16_t reserved3;
        std::uint64_t logicalAddress;
        std::uint32_t recvBufferDescrAddress;
        std::uint32_t sendBufferDescrAddress;
    } __attribute__((packed));

    struct BufferDescriptor
    {
        std::uint32_t address;
        std::uint32_t flags;
        std::uint32_t flags2;
        std::uint32_t avail;
    } __attribute__((packed));

public:
    amd_am79c973(const hwcomm::PCIDeviceDescr &dev,
                 hwcomm::InterruptManager &interrupts);

public:
    virtual void send(span<const std::uint8_t> msg) override;
    virtual void receive() override;
    virtual NetOrder<std::uint64_t> getMAC() const override;

private:
    virtual void activate() override;
    virtual int reset() override;
    virtual const char * getName() const override;

private:
    virtual std::uint32_t handleInterrupt(std::uint32_t esp) override;

private:
    hwcomm::Port16 macAddress0Port;
    hwcomm::Port16 macAddress2Port;
    hwcomm::Port16 macAddress4Port;
    hwcomm::Port16 registerDataPort;
    hwcomm::Port16 registerAddressPort;
    hwcomm::Port16 resetPort;
    hwcomm::Port16 busControlRegisterDataPort;

    InitializationBlock initBlock;

    BufferDescriptor *sendBufferDescr;
    std::uint8_t sendBufferDescrMem[2048 + 15];
    std::uint8_t sendBuffers[2*1024 + 15][8];
    std::uint8_t currentSendBuffer;

    BufferDescriptor *recvBufferDescr;
    std::uint8_t recvBufferDescrMem[2048 + 15];
    std::uint8_t recvBuffers[2*1024 + 15][8];
    std::uint8_t currentRecvBuffer;
};

}

#endif // TOS__DRIVERS__AMD_AM79C973_HPP__
