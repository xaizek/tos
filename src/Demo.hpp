#ifndef TOS__DEMO_HPP__
#define TOS__DEMO_HPP__

#include "drv/VGA.hpp"
#include "drv/keyboard.hpp"
#include "drv/mouse.hpp"
#include "gui/Desktop.hpp"
#include "gui/Window.hpp"
#include "hwcomm/Screen.hpp"
#include "print.hpp"

class Demo : public drv::KeyboardEventHandler, public drv::MouseEventHandler
{
    enum class Stage
    {
        Start,
        Console,
        Graphics,
    };

public:
    Demo() : desktop({320,200}, {0x00,0x00,0xA8}),
             win1(&desktop, { 10, 10 }, { 20, 20 }, {0xa8,0x00,0x00}),
             win2(&desktop, { 40, 15 } , { 30, 30 }, {0x00,0xa8,0x00}),
             win3(&win2, { 5, 5 } , { 10, 10 }, {0x00,0x00,0x00})
    {
        desktop.addChild(win1);
        win2.addChild(win3);
        desktop.addChild(win2);
    }

public:
    void run()
    {
        syskprint("\nGo into text or graphic mode? (t/g)\n");

        while (lastKey != 'g' && lastKey != 't') {
            asm volatile("hlt");
        }

        if (lastKey == 'g') {
            goGraphics();
        } else {
            goConsole();
        }
    }

private:
    virtual void onKeyDown(std::uint8_t scanCode) override
    {
        switch (stage) {
            case Stage::Start:    lastKey = mapScanCode(scanCode); break;
            case Stage::Console:  return console.onKeyDown(scanCode);
            case Stage::Graphics: return desktop.onKeyDown(scanCode);
        }
    }

private:
    virtual void onActivate() override
    {
        switch (stage) {
            case Stage::Start:    return;
            case Stage::Console:  return console.onActivate();
            case Stage::Graphics: return desktop.onActivate();
        }
    }

    virtual void onMouseMove(int dx, int dy) override
    {
        switch (stage) {
            case Stage::Start:    return;
            case Stage::Console:  return console.onMouseMove(dx, dy);
            case Stage::Graphics: return desktop.onMouseMove(dx, dy);
        }
    }

    virtual void onMouseDown(std::uint8_t button) override
    {
        switch (stage) {
            case Stage::Start:    return;
            case Stage::Console:  return console.onMouseDown(button);
            case Stage::Graphics: return desktop.onMouseDown(button);
        }
    }

    virtual void onMouseUp(std::uint8_t button) override
    {
        switch (stage) {
            case Stage::Start:    return;
            case Stage::Console:  return console.onMouseUp(button);
            case Stage::Graphics: return desktop.onMouseUp(button);
        }
    }

private:
    void syskprint(const char str[])
    {
        asm("int $0x80" : : "a" (4), "b" (str));
    }

    void goConsole()
    {
        stage = Stage::Console;
        getScreen().clear();
        onActivate();

        syskprint("Text mode\n");
        syskprint("Type and move mouse around.\n");
    }

    void goGraphics()
    {
        stage = Stage::Graphics;
        onActivate();
        vga.setMode(drv::Mode { 320, 200, 8 });
        while (true) {
            vga.beginDrawing();
            desktop.draw(vga);
            vga.endDrawing();
            asm volatile("hlt");
        }
    }

private:
    Console console;
    drv::VGA vga;
    char lastKey = '\0';
    Stage stage = Stage::Start;

    gui::Desktop desktop;
    gui::Window win1, win2, win3;
};

#endif // TOS__DEMO_HPP__
