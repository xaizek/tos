#ifndef TOS__CONSOLE_HPP__
#define TOS__CONSOLE_HPP__

#include <cstdint>

#include "drv/keyboard.hpp"
#include "drv/mouse.hpp"

class Console : public drv::KeyboardEventHandler, public drv::MouseEventHandler
{
public:
    Console();

public:
    virtual void onKeyDown(std::uint8_t scanCode) override;

public:
    virtual void onActivate() override;
    virtual void onMouseMove(int dx, int dy) override;

private:
    std::int8_t mouseX, mouseY;
};

char mapScanCode(std::uint8_t scanCode);

#endif // TOS__CONSOLE_HPP__
