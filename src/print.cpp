#include "print.hpp"

#include <cstdint>

#include "hwcomm/Screen.hpp"

hwcomm::Screen &
getScreen()
{
    static hwcomm::Screen screen;
    return screen;
}

void
detail::kprint(const char str[])
{
    getScreen().print(str);
}

void
detail::kprint(std::uint8_t byte)
{
    static const char hex[] = "0123456789ABCDEF";

    char msg[] = "00";
    msg[0] = hex[(byte >> 4) & 0xF];
    msg[1] = hex[byte & 0xF];
    detail::kprint(msg);
}

void
detail::kprint(std::uint16_t word)
{
    detail::kprint(static_cast<std::uint8_t>(word >> 8));
    detail::kprint(static_cast<std::uint8_t>(word & 0xff));
}

void
detail::kprint(std::uint32_t dword)
{
    detail::kprint(static_cast<std::uint16_t>(dword >> 16));
    detail::kprint(static_cast<std::uint16_t>(dword & 0xffff));
}

void
detail::kprint(std::uint64_t dword)
{
    detail::kprint(static_cast<std::uint32_t>(dword >> 32));
    detail::kprint(static_cast<std::uint32_t>(dword & 0xffffffff));
}

void
detail::kprint(void *ptr)
{
    detail::kprint(reinterpret_cast<std::uintptr_t>(ptr));
}
