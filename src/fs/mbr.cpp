#include "fs/mbr.hpp"

#include "drv/ATA.hpp"
#include "fs/fat.hpp"
#include "fs/mbr.hpp"
#include "print.hpp"

using namespace fs;

void
MBR::readPartitions(drv::ATA &hd)
{
    kprint("MBR: ");

    std::uint8_t mbrSector[512];
    hd.read28(0, mbrSector);

    MasterBootRecord mbr(mbrSector);

    if (mbr.magicnumber != 0xAA55) {
        kprint("illegal MBR");
        return;
    }

    kprint("\n");
    for (std::uint8_t i = 0; i < 4; ++i) {
        const std::uint8_t id = mbr.primaryPartition[i]->partition_id;
        if (id == 0x00) {
            continue;
        }

        const char *is = mbr.primaryPartition[i]->bootable == 0x80 ? ""
                                                                   : " not";
        kprint(" Partition ", i, is, " bootable.  Type 0x", id, ".\n");

        FAT fat(hd, mbr.primaryPartition[i]->start_lba);
        fat.printFiles();
    }
}
