#ifndef TOS__FILESYSTEM__MSDOSPART_HPP__
#define TOS__FILESYSTEM__MSDOSPART_HPP__

#include <cstdint>

#include "utils/struct.hpp"

namespace drv {
    class ATA;
}

namespace fs {

struct PartitionTableEntry
{
    Field<std::uint8_t> bootable;
    Field<std::uint32_t, 3> startCHS;
    Field<std::uint8_t> partition_id;
    Field<std::uint32_t, 3> endCHS;
    Field<std::uint32_t> start_lba;
    Field<std::uint32_t> length;

    explicit PartitionTableEntry(std::uint8_t *&data)
        : bootable(data), startCHS(data), partition_id(data), endCHS(data),
          start_lba(data), length(data)
    {
    }

    static constexpr std::ptrdiff_t size()
    {
        return 16;
    }
};

struct MasterBootRecord
{
    Fields<std::uint8_t, 440> bootloader;
    Field<std::uint32_t> signature;
    Field<std::uint16_t> unused;

    Fields<PartitionTableEntry, 4> primaryPartition;

    Field<std::uint16_t> magicnumber;

    explicit MasterBootRecord(std::uint8_t data[])
        : bootloader(data), signature(data), unused(data),
          primaryPartition(data), magicnumber(data)
    {
    }
};

class MBR
{
public:
    static void readPartitions(drv::ATA &hd);
};

}

#endif // TOS__FILESYSTEM__MSDOSPART_HPP__
