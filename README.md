This is an alternative version of sources presented as part of
[Write Your Own OS](http://wyoos.org/) video tutorial by Viktor Engelmann.

The sources were modified to be somewhat cleaner, more portable and with less
bugs.

There are still many issues and limitations and a lot of things can be improved
in the code base.

Makefile doesn't contain anything for VirtualBox and relies on qemu being
available.

### Differences from original ###

* Use of C++11
* Use of qemu (no iso images or grub is necessary)
* Fixed invocation of constructors of global objects
* Set alignment for multiboot header
* Added dependency tracking to the Makefile
* Flags and build target for running a debugger
* Fixed various weird or incorrect things
* Reduced number of UBs in the code (this results in more complicated and slow
  but more correct code; plus, it was quite fun to implement)
* Cosmetic changes like consistent naming, no commented out code, etc
* Make keyboard and mouse work at the same time
* No `include/` directory
* No outermost namespace for everything (it's not a library after all)
* Double buffering to avoid flickering of graphics

### Prerequisites ###

* g++
* GNU Make
* qemu for i386

### Trying it out ###

Starting the VM:

```
make run
```

Debugging:

```
make gdb
```

Testing network:

```
# start one of these before running the VM in a separate terminal
make udp-server
make tcp-server

# these can be run after VM has started (e.g. `make run; make udp-client`)
make udp-client
make tcp-client

# open http://127.0.0.1:1234/ in a browser
```

### Possible additions/changes ###

* Virtual memory
* User-mode
* SMP
* USB
* 64-bit mode
* Syscalls using `SYSENTER`
* Handle loopback in IP stack
* Graceful shutdown (ACPI)
* Support GPT

### License ###

GPLv3
